﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour
{
    // Start is called before the first frame update

    public static CharacterController instance;
    public int life;
    public int maxLife;
    public Text UIlife;

    private void Start()
    {
        maxLife = 3;
        life = maxLife;
    }

    private void Update()
    {
        UIlife = GameObject.Find("Life").GetComponent<Text>();
        UIlife.text = life.ToString();
    }
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void checkHealth()
    {
        life -= 1;
    }

}
