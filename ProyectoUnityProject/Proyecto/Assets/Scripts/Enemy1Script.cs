﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1Script : MonoBehaviour
{
    
    public GameObject projectile;
    public int position;
    private float espera;
    private Rigidbody2D rb2d;
    // Start is called before the first frame update
    void Start()
    {
        position = 0;
        rb2d = GetComponent<Rigidbody2D>();
    }
    public float ratioDisparo = 1f;
    private float _tiempoHastaElMomento = 0f;
    // Update is called once per frame
    void Update()
    {
        
        

        if (position==0)
        {
            espera += Time.deltaTime;
            if (espera>=0.5f)
            {
                //Se usa Instantiate con parametro el gameobject que quiero crear
                GameObject bala = GameObject.Instantiate(projectile);

                //esto para ponerle la posicion a mano
                bala.transform.position = this.transform.position + new Vector3(-0.30f, 0, 0);

                Rigidbody2D balaRigidbody = bala.GetComponent<Rigidbody2D>();
                balaRigidbody.velocity = new Vector3(-3f, 0, 0);
                espera = 0;
                position = 1;
            }
        }

        if (position==1)
        {
            espera += Time.deltaTime;
            if (espera>=1f)
            {
                rb2d.velocity = new Vector2(0, 3f);
            }
            if (espera >= 1.5f)
            {
                rb2d.velocity = new Vector2(0, 0);
                if (espera>=2f)
                {
                    //Se usa Instantiate con parametro el gameobject que quiero crear
                    GameObject bala = GameObject.Instantiate(projectile);

                    //esto para ponerle la posicion a mano
                    bala.transform.rotation = new Quaternion(0, 0, 0, 0);
                    bala.transform.position = this.transform.position + new Vector3(0, 0.30f, 0);

                    Rigidbody2D balaRigidbody = bala.GetComponent<Rigidbody2D>();
                    balaRigidbody.velocity = new Vector3(0, 3f, 0);

                    espera = 0;
                    position = 2;
                }

                
            }
        }

        if (position == 2)
        {
            espera += Time.deltaTime;
            if (espera >= 1f)
            {
                rb2d.velocity = new Vector2(3f, 0);
            }
            if (espera >= 1.5f)
            {
                rb2d.velocity = new Vector2(0, 0);
                if (espera >= 2f)
                {
                    //Se usa Instantiate con parametro el gameobject que quiero crear
                    GameObject bala = GameObject.Instantiate(projectile);

                    //esto para ponerle la posicion a mano
                    bala.transform.rotation = new Quaternion(0, 0, 0.7071068f, -0.7071068f);
                    bala.transform.position = this.transform.position + new Vector3(0.30f, 0, 0);

                    Rigidbody2D balaRigidbody = bala.GetComponent<Rigidbody2D>();
                    balaRigidbody.velocity = new Vector3(3, 0, 0);

                    espera = 0;
                    position = 3;
                }


            }
        }

        if (position == 3)
        {
            espera += Time.deltaTime;
            if (espera >= 1f)
                rb2d.velocity = new Vector2(0, -3f);
            if (espera >= 1.5f)
            {
                rb2d.velocity = new Vector2(0, 0);
                if (espera >= 2f)
                {
                    //Se usa Instantiate con parametro el gameobject que quiero crear
                    GameObject bala = GameObject.Instantiate(projectile);

                    //esto para ponerle la posicion a mano
                    bala.transform.rotation = new Quaternion(0, 0, 180, 0);
                    bala.transform.position = this.transform.position + new Vector3(0, -0.30f, 0);

                    Rigidbody2D balaRigidbody = bala.GetComponent<Rigidbody2D>();
                    balaRigidbody.velocity = new Vector3(0, -3, 0);

                    espera = 0;
                    position = 4;
                }


            }
        }

        if (position== 4)
        {
            espera += Time.deltaTime;
            if (espera >= 1f)
                rb2d.velocity = new Vector2(-3f, 0);
            if (espera >= 1.5f)
            {
                rb2d.velocity = new Vector2(0, 0);
                if (espera >= 2f)
                {
                    //Se usa Instantiate con parametro el gameobject que quiero crear
                    GameObject bala = GameObject.Instantiate(projectile);

                    //esto para ponerle la posicion a mano
                    bala.transform.position = this.transform.position + new Vector3(-0.30f, 0, 0);

                    Rigidbody2D balaRigidbody = bala.GetComponent<Rigidbody2D>();
                    balaRigidbody.velocity = new Vector3(-3, 0, 0);

                    espera = 0;
                    position = 1;
                }


            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        collision.gameObject.GetComponent<CharacterMvMnt>().death();
    }

    

}

