﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenusScript : MonoBehaviour
{
    public static MenusScript instance;
    public GameObject character;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }

    

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void play()
    {
        SceneManager.LoadScene("Overworld");
        character = GameObject.FindGameObjectWithTag("character");
        character.GetComponent<SpriteRenderer>().enabled = true;
       
    }

    public void exit()
    {
        Application.Quit();
    }
}
