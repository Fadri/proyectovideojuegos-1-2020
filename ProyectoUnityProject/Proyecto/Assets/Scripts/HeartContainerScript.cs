﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartContainerScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag=="character")
        {
            CharacterController.instance.maxLife++;
            CharacterController.instance.life = CharacterController.instance.maxLife;
            Destroy(this.gameObject);
        }
    }
}
