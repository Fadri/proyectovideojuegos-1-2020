﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterMvMnt : MonoBehaviour
{
    public static CharacterMvMnt instance;
    public float MvMntSpeed=5f;
    private Rigidbody2D rb2d;
    public bool getSword=false;
    public GameObject sword;
    private int position;
    public bool soyYo=true;
    // Start is called before the first frame update

        
    void Start()
    {

        
        rb2d = GetComponent<Rigidbody2D>();
        

    }
     

    // Update is called once per frame
    void Update()
    {
        rb2d.velocity = new Vector2(0, 0);

        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb2d.velocity = new Vector2(3f, rb2d.velocity.y);
            position = 1;

        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb2d.velocity = new Vector2(-3f, rb2d.velocity.y);
            position = 2;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, 3f);
            position = 3;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, -3f);
            position = 4;
        }
        if (getSword)
        {

            if (Input.GetKeyDown(KeyCode.X))
            {

                if (position == 1)
                {
                    GameObject espada = GameObject.Instantiate(sword);
                    espada.transform.parent = this.transform;
                    espada.transform.position = this.transform.position + new Vector3(0.1f, 0);
                }
                if (position == 2)
                {
                    GameObject espada = GameObject.Instantiate(sword);
                    espada.transform.parent = this.transform;
                    espada.transform.position = this.transform.position + new Vector3(-0.1f, 0);
                    espada.transform.rotation = new Quaternion(0, 0, 1, 0);
                }
                if (position == 3)
                {
                    GameObject espada = GameObject.Instantiate(sword);
                    espada.transform.parent = this.transform;
                    espada.transform.position = this.transform.position + new Vector3(0, 0.1f);
                    espada.transform.rotation = new Quaternion(0, 0, 0.7071078f, 0.7071078f);
                }
                if (position == 4)
                {
                    GameObject espada = GameObject.Instantiate(sword);
                    espada.transform.parent = this.transform;
                    espada.transform.position = this.transform.position + new Vector3(0, -0.1f);
                    espada.transform.rotation = new Quaternion(0, 0, -0.7071078f, 0.7071078f);
                }


            }

        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            this.GetComponent<SpriteRenderer>().enabled = false;
            SceneManager.LoadScene("MainMenu");
        }

        
    }
    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public void death()
    {
        CharacterController.instance.checkHealth();
        if (CharacterController.instance.life<=0)
        {
            Destroy(this.gameObject);
            SceneManager.LoadScene("Overworld");
        }
    }
}
