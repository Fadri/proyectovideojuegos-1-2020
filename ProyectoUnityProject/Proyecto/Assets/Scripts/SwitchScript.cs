﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchScript : MonoBehaviour
{

    private void Update()
    {
        if (SwitchManager.instance.Switch)
        {
            GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);
        }
        else
        {
            GetComponent<SpriteRenderer>().color = new Color(0, 0, 255);
        }

    }
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "sword")
        {
            
            if (SwitchManager.instance.checkSwitch())
            {

                GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);

            }
            else
            {
                GetComponent<SpriteRenderer>().color = new Color(0, 0, 255);
            }

        }
    }


}
