﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonRoomScript : MonoBehaviour
{
    public LayerMask LoadingArea;
    public BoxCollider2D AreaaRevisar;
    public GameObject door;
    public GameObject heartContainer;
    public GameObject target;
    public GameObject enemy2;
    public float speed = 2f;
    public bool estaAhi = false;
    public GameObject projectile;

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("character");
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!playerEnter())
        {
            door.SetActive(false);
            heartContainer.SetActive(true);
        }
        if (estaAhi)
        {
            enemy2.transform.position = Vector3.MoveTowards(enemy2.transform.position, target.transform.position, Time.deltaTime * speed);
        }
        

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag=="character")
        {
            estaAhi = true;
            door.SetActive(true);
            
        }
        
        

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        estaAhi = false;
    }

    public bool playerEnter()
    {
        RaycastHit2D[] puntosEncontrados = Physics2D.BoxCastAll(AreaaRevisar.gameObject.transform.position, AreaaRevisar.bounds.size, 0f, Vector2.zero, 0f, LoadingArea.value);
        return puntosEncontrados.Length > 0;
    }
}
