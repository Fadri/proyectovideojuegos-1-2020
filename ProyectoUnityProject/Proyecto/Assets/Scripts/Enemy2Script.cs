﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2Script : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform target;
    public GameObject enemy2;
    public float speed = 2f;
    public bool estaAhi=false;
    private void OnTriggerEnter2D(Collider2D collision)
    {

        estaAhi = collision.GetComponent<CharacterMvMnt>().soyYo;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        estaAhi = false;
    }

    private void Update()
    {
        if (estaAhi)
        {
            enemy2.transform.position = Vector3.MoveTowards(this.transform.position, target.position, Time.deltaTime * speed);
        }
    }


}
