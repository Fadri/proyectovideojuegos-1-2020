﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BoxCastLevelLoader : MonoBehaviour
{
    // Start is called before the first frame update
    public LayerMask LoadingArea;
    public BoxCollider2D AreaaRevisar;
    public String LevelToLoad;
    public Camera house1;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (playerEnter())
        {
            LoadScene();
        }
        
    }

    private void LoadScene()
    {
        
        SceneManager.LoadScene(LevelToLoad);
        
        
    }

    private bool playerEnter()
    {
        bool result = false;
        RaycastHit2D[] puntosEncontrados = Physics2D.BoxCastAll(AreaaRevisar.gameObject.transform.position, AreaaRevisar.bounds.size,0f,Vector2.zero,0f,LoadingArea.value);
        return puntosEncontrados.Length>0;
    }
}
