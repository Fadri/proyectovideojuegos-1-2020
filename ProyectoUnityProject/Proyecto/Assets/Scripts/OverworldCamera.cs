﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverworldCamera : MonoBehaviour
{
    public static OverworldCamera instance;
    public  GameObject Character;
    private Vector2 Charpos;
    private Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        Character = GameObject.FindGameObjectWithTag("character");
        
    }

    // Update is called once per frame
    void Update()
    {

        transform.position = new Vector3(Character.transform.position.x, Character.transform.position.y, -10);
        
    }

   
}
