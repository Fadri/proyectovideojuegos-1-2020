﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueGateScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = !SwitchManager.instance.Switch;
        this.transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = !SwitchManager.instance.Switch;
        this.transform.GetChild(2).GetComponent<SpriteRenderer>().enabled = !SwitchManager.instance.Switch;
        this.transform.GetChild(3).GetComponent<SpriteRenderer>().enabled = !SwitchManager.instance.Switch;
        this.transform.GetChild(4).GetComponent<SpriteRenderer>().enabled = !SwitchManager.instance.Switch;
        this.GetComponent<BoxCollider2D>().enabled = !SwitchManager.instance.Switch;

    }
}
