﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordGet : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        collision.GetComponent<CharacterMvMnt>().getSword = true;
        Destroy(this.gameObject);
    }
}
