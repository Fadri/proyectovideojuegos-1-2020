﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchManager : MonoBehaviour
{
    // Start is called before the first frame update

    public static SwitchManager instance;
    public bool Switch;
    public GameObject RedGate;
    public GameObject BlueGate;

    private void Start()
    {
        Switch = true;
    }

    

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }

    }

    public bool checkSwitch()
    {
        if (Switch)
        {
            openGate();
            return Switch;
        }
        else
        {
            closeGate();
            return Switch;
        }
    }

    public void openGate()
    {
        Switch = false;
    }

    public void closeGate()
    {
        Switch = true;
    }
}


